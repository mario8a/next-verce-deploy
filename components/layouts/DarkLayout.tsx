import { FC } from "react"

type Props = {
  children: React.ReactNode;
}

const DarkLayout:FC<Props> = ({ children }) => {
  return (
    <div style={{backgroundColor: 'rgba(0,0,0,0.3)', borderRadius: '5px', padding: '10px'}}>
      <h1>Dark layout</h1>
      <div>
        {children}
      </div>
    </div>
  )
}

export default DarkLayout